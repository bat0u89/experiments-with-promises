
let i = 0;
let promise: Promise<string> = new Promise((resolve, reject) => {
    console.log("executor started");
    i = i + 1;
    setTimeout(() => resolve("a string from promise " + i), 1000);
});

promise
    .then(input => {
    console.log("then has worked 1: " + input);
    const randomInt = Date.now();
    const value = randomInt % 10;
    if (value < 10) {
        throw new Error("error thrown because value was: " + value);
    }
    return 420;
    })
    .catch((err: Error) => {
    console.log("error was caught: " + err.message);
    return 24;
    })
    .then(input => console.log("chained then 1: " + input));
promise.then(input => console.log("then has worked 2: " + input));
promise.then(input => console.log("then has worked 3: " + input));

Promise.all([
    () => console.log("hello1"),
    () => console.log("hello2"),
    () => console.log("hello3")
]).then(([result1, result2, result3]) => {
    console.log(`results are: ${result1}, ${result2}, ${result3}`);
});

[
    val => {
    console.log("val: " + val);
    return 1;
    },
    val => {
    console.log("val: " + val);
    return 2;
    },
    val => {
    console.log("val: " + val);
    return 3;
    }
]
    .reduce((accumulated, f) => {
    return accumulated.then(f);
    }, Promise.resolve())
    .then(result3 => {
    console.log(`result3: ${result3}`);
    });


Promise.all(["hello1", "hello2", "hello3"]).then(
    ([result1, result2, result3]) => {
    console.log(`results are: ${result1}, ${result2}, ${result3}`);
    }
);

Promise.all([
    new Promise(resolve => {
    resolve("hello1");
    }).then(str => str + "-"),
    new Promise(resolve => {
    resolve("hello2");
    }),
    new Promise(resolve => {
    resolve("hello3");
    })
]).then(([result1, result2, result3]) => {
    console.log(`results are: ${result1}, ${result2}, ${result3}`);
});

const chainPromiseWithFunction = (
    accumulatorPromise: Promise<any>,
    func: (val: any) => any
) => accumulatorPromise.then(func);
const composeAsync = (...funcs) => seedVal =>
    funcs.reduce(chainPromiseWithFunction, Promise.resolve(seedVal));
composeAsync(
    x => {
    console.log(`I'm x: ${x}`);
    return x + 1;
    },
    x => {
    console.log(`I'm x: ${x}`);
    return x + 2;
    },
    x => {
    console.log(`I'm x: ${x}`);
    return x + 3;
    },
    x => {
    console.log(`I'm x: ${x}`);
    }
)(1);
const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

wait(3000).then(() => {
    throw new Error("oops!");
});
Promise.resolve()
    .then(() => console.log(2))
    .then(() => console.log(3));
console.log(1); // 1, 2, 3, 4
